README

Should you wish to run this code, please follow these instructions:
1. Clone repo/download "EEE4114F_ML_Project.ipynb" and dataset.zip
2. Extract dataset.zip into the current directory
3. Open and run the notebook

Note, the location of the dataset folder must be in the same directory and level as the ipynb notebook or it will fail to load the data.

Thank You :)
